package main

import (
	"github.com/aws/aws-cdk-go/awscdk/v2"
	"github.com/aws/aws-cdk-go/awscdk/v2/awsevents"
	"github.com/aws/aws-cdk-go/awscdk/v2/awseventstargets"
	"github.com/aws/aws-cdk-go/awscdk/v2/awsiam"
	"github.com/aws/aws-cdk-go/awscdk/v2/awslambda"
	"github.com/aws/aws-cdk-go/awscdk/v2/awssqs"
	"github.com/aws/jsii-runtime-go"

	// "github.com/aws/aws-cdk-go/awscdk/v2/awssqs"
	"github.com/aws/constructs-go/constructs/v10"
	// "github.com/aws/jsii-runtime-go"
)

type O2CEventDrivenGolangStackProps struct {
	awscdk.StackProps
}

const ORDERS_EVENT_BUS = "Orders"

func NewO2CEventDrivenGolangStack(scope constructs.Construct, id string, props *O2CEventDrivenGolangStackProps) awscdk.Stack {
	var sprops awscdk.StackProps
	if props != nil {
		sprops = props.StackProps
	}
	stack := awscdk.NewStack(scope, &id, &sprops)

	ordersBus := awsevents.NewEventBus(stack, jsii.String("OrdersEventBus"), &awsevents.EventBusProps{
		EventBusName: jsii.String(ORDERS_EVENT_BUS),
	})

	newOrderFunction := awslambda.NewFunction(stack, jsii.String("CreateOrderFunction"), &awslambda.FunctionProps{
		Runtime: awslambda.Runtime_GO_1_X(),
		Handler: jsii.String("functions"),
		Code:    awslambda.AssetCode_FromAsset(jsii.String("out"), nil),
		Environment: &map[string]*string{
			"ORDERS_EVENT_BUS": jsii.String(ORDERS_EVENT_BUS),
			"LAMBDA_FUNCTION":  jsii.String("CreateOrderFunction"),
		},
	})

	newOrderFunctionPolicy := awsiam.NewPolicy(stack, jsii.String("NewOrderFunctionPolicy"), &awsiam.PolicyProps{
		Statements: &[]awsiam.PolicyStatement{
			awsiam.NewPolicyStatement(&awsiam.PolicyStatementProps{
				Actions:   jsii.Strings("events:PutEvents"),
				Resources: jsii.Strings(*ordersBus.EventBusArn()),
			}),
		},
	})

	newOrderFunction.Role().AttachInlinePolicy(newOrderFunctionPolicy)

	provisioningFunction := awslambda.NewFunction(stack, jsii.String("ProvisioningFunction"), &awslambda.FunctionProps{
		Runtime: awslambda.Runtime_GO_1_X(),
		Handler: jsii.String("functions"),
		Code:    awslambda.AssetCode_FromAsset(jsii.String("out"), nil),
		Environment: &map[string]*string{
			"ORDERS_EVENT_BUS": jsii.String(ORDERS_EVENT_BUS),
			"LAMBDA_FUNCTION":  jsii.String("ProvisioningFunction"),
		},
	})

	newOrderRule := awsevents.NewRule(stack, jsii.String("NewOrderRule"), &awsevents.RuleProps{
		Schedule: awsevents.Schedule_Expression(jsii.String("rate(1 day)")),
	})

	provisionOrderRule := awsevents.NewRule(stack, jsii.String("ProvisionOrderRule"), &awsevents.RuleProps{
		EventBus: ordersBus,
		EventPattern: &awsevents.EventPattern{
			Source:     jsii.Strings("com.gitlab.creator"),
			DetailType: jsii.Strings("orderCreated"),
		},
	})

	dlq := awssqs.NewQueue(stack, jsii.String("ProvisionOrderDLQ"), &awssqs.QueueProps{})

	newOrderRule.AddTarget(awseventstargets.NewLambdaFunction(newOrderFunction, &awseventstargets.LambdaFunctionProps{}))

	provisionOrderRule.AddTarget(awseventstargets.NewLambdaFunction(provisioningFunction, &awseventstargets.LambdaFunctionProps{
		DeadLetterQueue: dlq,
		RetryAttempts:   jsii.Number(3),
	}))

	return stack
}

func main() {
	app := awscdk.NewApp(nil)

	NewO2CEventDrivenGolangStack(app, "O2CEventDrivenGolangStack", &O2CEventDrivenGolangStackProps{
		awscdk.StackProps{
			Env: env(),
		},
	})

	app.Synth(nil)
}

// env determines the AWS environment (account+region) in which our stack is to
// be deployed. For more information see: https://docs.aws.amazon.com/cdk/latest/guide/environments.html
func env() *awscdk.Environment {
	// If unspecified, this stack will be "environment-agnostic".
	// Account/Region-dependent features and context lookups will not work, but a
	// single synthesized template can be deployed anywhere.
	//---------------------------------------------------------------------------
	return nil

	// Uncomment if you know exactly what account and region you want to deploy
	// the stack to. This is the recommendation for production stacks.
	//---------------------------------------------------------------------------
	// return &awscdk.Environment{
	//  Account: jsii.String("123456789012"),
	//  Region:  jsii.String("us-east-1"),
	// }

	// Uncomment to specialize this stack for the AWS Account and Region that are
	// implied by the current CLI configuration. This is recommended for dev
	// stacks.
	//---------------------------------------------------------------------------
	// return &awscdk.Environment{
	//  Account: jsii.String(os.Getenv("CDK_DEFAULT_ACCOUNT")),
	//  Region:  jsii.String(os.Getenv("CDK_DEFAULT_REGION")),
	// }
}
