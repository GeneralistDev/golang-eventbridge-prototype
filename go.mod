module o2c-event-driven-golang

go 1.17

require (
	github.com/aws/aws-cdk-go/awscdk/v2 v2.10.0
	github.com/aws/aws-lambda-go v1.28.0
	github.com/aws/aws-sdk-go-v2/service/eventbridge v1.13.0
	github.com/aws/constructs-go/constructs/v10 v10.0.9
	github.com/aws/jsii-runtime-go v1.52.1
)

require (
	github.com/Masterminds/semver/v3 v3.1.1 // indirect
	github.com/aws/aws-sdk-go-v2 v1.13.0 // indirect
	github.com/aws/aws-sdk-go-v2/internal/configsources v1.1.4 // indirect
	github.com/aws/aws-sdk-go-v2/internal/endpoints/v2 v2.2.0 // indirect
	github.com/aws/smithy-go v1.10.0 // indirect
)
