package main

import (
	"github.com/aws/aws-lambda-go/lambda"
	create_order "lambda.functions/create-order"
	provision_order "lambda.functions/provision-order"
	"log"
	"os"
)

func main() {
	switch os.Getenv("LAMBDA_FUNCTION") {
	case "CreateOrderFunction":
		lambda.Start(create_order.Handler)
	case "ProvisioningFunction":
		lambda.Start(provision_order.Handler)
	default:
		log.Fatalln("LAMBDA_FUNCTION did not match any known function")
	}
}
