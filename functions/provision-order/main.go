package provision_order

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/eventbridge"
	"github.com/aws/aws-sdk-go-v2/service/eventbridge/types"
	"github.com/aws/jsii-runtime-go"
	"log"
	"os"
)

func Handler(ctx context.Context, event interface{}) {
	eventData, _ := json.Marshal(event)
	fmt.Println(string(eventData))

	cfg, err := config.LoadDefaultConfig(ctx)

	if err != nil {
		log.Fatal(err)
	}

	client := eventbridge.NewFromConfig(cfg)

	json, _ := json.Marshal(map[string]string{
		"orderId": "1234",
		"status":  "provisioned",
	})

	params := eventbridge.PutEventsInput{
		Entries: []types.PutEventsRequestEntry{
			{
				Detail:       jsii.String(string(json)),
				DetailType:   jsii.String("orderProvisioned"),
				EventBusName: jsii.String(os.Getenv("ORDERS_EVENT_BUS")),
				Source:       jsii.String("com.gitlab.provisioner"),
			},
		},
	}

	client.PutEvents(ctx, &params)
}
