build:
	cd functions; \
	GOOS=linux GOARCH=amd64 go build -o ../out/functions .

cdkdeploy:
	cdk deploy

deploy: build cdkdeploy
